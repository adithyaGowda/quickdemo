package com.abc.qwerty.controller;

import com.abc.qwerty.domain.Example;
import com.abc.qwerty.service.ExampleServ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1")
public class ExampleController {
    private ExampleServ exampleServ;

    @Autowired
    public ExampleController(ExampleServ exampleServ) {
        this.exampleServ = exampleServ;
    }

    @PostMapping("post")
    public ResponseEntity<?> saveData(@RequestBody Example example){
        return new ResponseEntity<Example>(exampleServ.saveData(), HttpStatus.CREATED);
    }


}
