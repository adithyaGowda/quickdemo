package com.abc.qwerty.service;

import com.abc.qwerty.domain.Example;

public interface ExampleServ {
    Example saveData(Example example);
}
