package com.abc.qwerty.service;

import com.abc.qwerty.domain.Example;
import com.abc.qwerty.repository.ExampleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExampleServImpl implements ExampleServ {

    private ExampleRepository exampleRepository;

    @Autowired
    public ExampleServImpl(ExampleRepository exampleRepository) {
        this.exampleRepository = exampleRepository;
    }
    
    @Override
    public Example saveData(Example example) {
        return exampleRepository.save(example);
    }
}
