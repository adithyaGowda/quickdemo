package com.abc.qwerty.domain;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Getter
@Setter
@Entity
public class Example {
    @Id
    private int id;
    private String name;
}
