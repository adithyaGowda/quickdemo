package com.abc.qwerty.repository;

import com.abc.qwerty.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExampleRepository extends JpaRepository<Example,Integer> {
}
